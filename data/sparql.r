SparqlMetadata <- 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX dcterms: <http://purl.org/dc/terms/>

SELECT ?Name ?Creator ?Publisher ?AccuracyAndReliability ?Comment ?DateIssued ?DateModified ?Description
WHERE {

?obs rdfs:label "Local Authority Services and Performance - SHS".
?obs rdfs:label ?Name.

?obs dcterms:creator ?cre.
?cre rdfs:label ?Creator.

?obs dcterms:publisher ?pub.
?pub rdfs:label ?Publisher.  

?obs <http://statistics.gov.scot/def/statistical-quality/accuracy-and-reliability> ?AccuracyAndReliability.
?obs rdfs:comment ?Comment.
?obs dcterms:issued ?DateIssued.
?obs dcterms:modified ?DateModified.
?obs dcterms:description ?Description.
}'

SparqlData <- 'PREFIX qb: <http://purl.org/linked-data/cube#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX dim: <http://statistics.gov.scot/def/dimension/>
PREFIX sdmx: <http://purl.org/linked-data/sdmx/2009/dimension#>

SELECT ?year ?age ?gender ?servicesperformance ?simd ?urbanrural ?area ?95lowerCI ?percent ?95upperCI

WHERE { 
?s qb:dataSet <http://statistics.gov.scot/data/local-authority-services-and-performance---shs>; 
dim:age ?ageURI; 
sdmx:refPeriod ?yearURI; 
dim:gender ?genderURI; 
dim:localAuthorityServicesAndPerformance ?servicesperformanceURI;
dim:simdQuintiles ?simdURI;
dim:urbanRuralClassification ?urbanruralURI;
<http://statistics.gov.scot/def/measure-properties/95-lower-confidence-limit-percent> ?95lowerCI; 
sdmx:refArea ?areaURI. 

?t qb:dataSet <http://statistics.gov.scot/data/local-authority-services-and-performance---shs>; 
dim:age ?ageURI; 
sdmx:refPeriod ?yearURI; 
dim:gender ?genderURI; 
dim:localAuthorityServicesAndPerformance ?servicesperformanceURI;
dim:simdQuintiles ?simdURI;
dim:urbanRuralClassification ?urbanruralURI;
<http://statistics.gov.scot/def/measure-properties/95-upper-confidence-limit-percent> ?95upperCI; 
sdmx:refArea ?areaURI. 

?u qb:dataSet <http://statistics.gov.scot/data/local-authority-services-and-performance---shs>; 
dim:age ?ageURI; 
sdmx:refPeriod ?yearURI; 
dim:gender ?genderURI; 
dim:localAuthorityServicesAndPerformance ?servicesperformanceURI;
dim:simdQuintiles ?simdURI;
dim:urbanRuralClassification ?urbanruralURI;
<http://statistics.gov.scot/def/measure-properties/percent> ?percent; 
sdmx:refArea ?areaURI. 

?ageURI rdfs:label ?age. 
?yearURI rdfs:label ?year. 
?genderURI rdfs:label ?gender. 
?servicesperformanceURI rdfs:label ?servicesperformance. 
?simdURI rdfs:label ?simd. 
?urbanruralURI rdfs:label ?urbanrural. 
?areaURI rdfs:label ?area.
} 

ORDER BY ?year'