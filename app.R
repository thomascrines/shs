library(shiny)
library(plotly)
library(reshape2)
library(leaflet)
library(jsonlite)
library(shinyjs)
library(rgdal)
library(plyr)
library(RColorBrewer)

source("source/variables.r")$value

ui <- source("source/ui.r", local = TRUE)$value

server <- function(input, output, session) {
  
  source("source/server_time_series.r", local = TRUE)$value
  
  source("source/server_by_year.r", local = TRUE)$value
  
  #TO DO: Remove banner for later releases
  shinyjs::onclick("close-banner", shinyjs::hide(id = "welcome-banner", anim = TRUE))
  
}

shinyApp(ui, server)
