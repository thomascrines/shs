SHS_BC_Scotland <- reactive ({
  
  Filtered <- Unfiltered %>% filter(Age == "All"
                                    & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceBC"
                                    & Gender == "All"
                                    & FeatureCode == "Scotland"
                                    & Urban.Rural.Classification == "All" 
                                    & SIMD.quintiles == "All"
                                    & DateCode == input$"DateCodeBC")
  
  data.frame(Filtered$Value, Filtered$Measurement)
  
})

SHS_BC <- reactive ({
  
  Filtered <- Unfiltered %>% filter(Age == "All"
                                    & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceBC"
                                    & Gender == "All"
                                    & FeatureCode != "Scotland"
                                    & Urban.Rural.Classification == "All" 
                                    & SIMD.quintiles == "All"
                                    & DateCode == input$"DateCodeBC")
  
  df <- data.frame(Filtered$DateCode
                   , Filtered$Local.Authority.Services.and.Performance
                   , Filtered$Value
                   , Filtered$FeatureCode
                   , Filtered$Measurement
  )
  
  SHS_BC <- tidyr::spread(df, Filtered.Measurement, Filtered.Value)
  
  SHS_BC <- cbind(StatisticallySignificant = "None", SHS_BC)
  
  levels(SHS_BC$StatisticallySignificant) <-c("None", "Significantly Higher", "Significantly Lower", "No Significant Difference")
  
  SHS_BC[which(SHS_BC$`95% Lower Confidence Limit, Percent` > (SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Upper Confidence Limit, Percent"),"Filtered.Value"])), "StatisticallySignificant"] <- "Significantly Higher"
  
  SHS_BC[which(SHS_BC$`95% Upper Confidence Limit, Percent` < (SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Lower Confidence Limit, Percent"),"Filtered.Value"])), "StatisticallySignificant"] <- "Significantly Lower"
  
  SHS_BC[which(SHS_BC$StatisticallySignificant == "None"), "StatisticallySignificant"] <- "No Significant Difference"
  
  SHS_BC
  
})
