SHS_LC <- reactive ({
  
  if(input$"VariableSelectLC" == "Local Authorities") {
    
    Filtered <- Unfiltered %>% filter(FeatureCode %in% c(input$"FeatureCodeLCMultiple")
                                      & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceLC"
                                      & Age == "All"
                                      & Gender == "All"
                                      & Urban.Rural.Classification == "All"
                                      & SIMD.quintiles == "All")
    
    df <- data.frame(Filtered$DateCode
                     , Filtered$Local.Authority.Services.and.Performance
                     , Filtered$Value
                     , Filtered$FeatureCode
                     , Filtered$Measurement)
    
  } else if (input$"VariableSelectLC" == "Age") {
    
    Filtered <- Unfiltered %>% filter(FeatureCode == input$"FeatureCodeLCSingle"
                                      & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceLC"
                                      & Gender == "All"
                                      & Urban.Rural.Classification == "All"
                                      & SIMD.quintiles == "All")
    
    df <- data.frame(Filtered$DateCode
                     , Filtered$Local.Authority.Services.and.Performance
                     , Filtered$Value
                     , Filtered$FeatureCode
                     , Filtered$Measurement
                     , Filtered$Age)
    
  } else if (input$"VariableSelectLC" == "Gender") {
    
    Filtered <- Unfiltered %>% filter(FeatureCode == input$"FeatureCodeLCSingle"
                                      & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceLC"
                                      & Age == "All"
                                      & Urban.Rural.Classification == "All"
                                      & SIMD.quintiles == "All")
    
    df <- data.frame(Filtered$DateCode
                     , Filtered$Local.Authority.Services.and.Performance
                     , Filtered$Value
                     , Filtered$FeatureCode
                     , Filtered$Measurement
                     , Filtered$Gender)
    
  } else if (input$"VariableSelectLC" == "Urban Rural Classification") {
    
    Filtered <- Unfiltered %>% filter(FeatureCode == input$"FeatureCodeLCSingle"
                                      & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceLC"
                                      & Gender == "All"
                                      & Age == "All"
                                      & SIMD.quintiles == "All")
    
    df <- data.frame(Filtered$DateCode
                     , Filtered$Local.Authority.Services.and.Performance
                     , Filtered$Value
                     , Filtered$FeatureCode
                     , Filtered$Measurement
                     , Filtered$Urban.Rural.Classification)
    
  } else if (input$"VariableSelectLC" == "SIMD Quintiles") {
    
    Filtered <- Unfiltered %>% filter(FeatureCode == input$"FeatureCodeLCSingle"
                                      & Local.Authority.Services.and.Performance == input$"LocalAuthorityServicesAndPerformanceLC"
                                      & Gender == "All"
                                      & Urban.Rural.Classification == "All"
                                      & Age == "All")
    
    df <- data.frame(Filtered$DateCode
                     , Filtered$Local.Authority.Services.and.Performance
                     , Filtered$Value
                     , Filtered$FeatureCode
                     , Filtered$Measurement
                     , Filtered$SIMD.quintiles)
  }
  
  df <- tidyr::spread(df
                      , Filtered.Measurement
                      , Filtered.Value)
  
  df <- rename(df, c("Filtered.DateCode" = "Year"
                     , "Filtered.Age" = "Age"
                     , "Filtered.Gender" = "Gender"
                     , "Filtered.SIMD.quintiles" = "SIMDQuintiles"
                     , "Filtered.Urban.Rural.Classification" = "UrbanRuralClassification"
                     , "Filtered.Local.Authority.Services.and.Performance" = "Statement"
                     , "Filtered.FeatureCode" = "LocalAuthority"
                     , "95% Lower Confidence Limit, Percent" = "LowerConfidenceLimit"
                     , "95% Upper Confidence Limit, Percent" = "UpperConfidenceLimit"))
  
})