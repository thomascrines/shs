tagList(
  
  useShinyjs(),
  
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "shs.css")),
  
  #TO DO: Remove banner for later releases
  tags$div(id="welcome-banner", "Welcome to the new Scottish Household Survey data visualisation site.",
           tags$br(),
           "We're still working on the site and welcome any comments and suggestions to ", 
          tags$a(href="mailto:shs@gov.scot", "shs@gov.scot"),
          actionButton("close-banner", "",icon=icon("times"))),
  
  navbarPage("Scottish Household Survey",
             
             source("source/tab_home.R", local = TRUE)$value,
             
             source("source/tab_time_series.R", local = TRUE)$value,
             
             source("source/tab_by_year.R", local = TRUE)$value,
             
             source("source/tab_about.R", local = TRUE)$value,
             
             fluid = TRUE
             
  ))
