source("source/data_proc_by_year.r", local = TRUE)

output$barChart <- renderPlotly({
  
  LowerConfidenceLimit <- SHS_BC()$"95% Lower Confidence Limit, Percent"
  UpperConfidenceLimit <- SHS_BC()$"95% Upper Confidence Limit, Percent"
  
  if (input$"OrderBC" == "Alphabetical") {
    
    LocalAuthority <- SHS_BC()$Filtered.FeatureCode
    
  } else if (input$"OrderBC" == "Ascending") {
    
    LocalAuthority <- reorder(SHS_BC()$Filtered.FeatureCode, SHS_BC()$Percent)
    
  } else if (input$"OrderBC" == "Descending") {
    
    LocalAuthority <- reorder(SHS_BC()$Filtered.FeatureCode, -SHS_BC()$Percent)
    
  }
  
  p <- ggplot(data=SHS_BC(), aes(x=LocalAuthority, y=Percent, fill=StatisticallySignificant)) +
    
    geom_bar(stat="identity", aes(text = paste("Value: ", Percent, "%","\n", 
                                           "Upper Confidence Limit: ", UpperConfidenceLimit,"%", "\n", 
                                           "Lower Confidence Limit: ", LowerConfidenceLimit, "%", "\n", 
                                           LocalAuthority, ",", Filtered.DateCode))) + 
    
    scale_fill_manual(values = byYearColours) +
    
    theme(axis.text.x = element_text(angle=90),
          legend.title = element_blank(),
          panel.grid.major.y = element_line(colour = "#b8b8ba", size = 0.3),
          panel.background = element_rect(fill = "transparent"),
          text = element_text(family = "Arial")) +
    
    labs(x = "local authority", 
         y = "percentage")
  
  if(input$zoomLevel_BC == "Full scale") {
    p <- p + ylim(0, 100)
  }
  
  else {
    p
  }
  
  
  if(input$"ConfidenceIntervalsBC" == TRUE & input$"ScottishAverageBC" == TRUE) {
    
    p + geom_rect(xmin=0, xmax=100
                , ymin=SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Upper Confidence Limit, Percent"),"Filtered.Value"]
                , ymax=SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Lower Confidence Limit, Percent"),"Filtered.Value"]
                , fill = "#008080"
                , alpha = 0.3) + 
      
      geom_hline(aes(yintercept = SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="Percent"),"Filtered.Value"],
                     text = paste("Scottish Average: ", SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="Percent"),"Filtered.Value"], "%","\n", 
                                  "Upper Confidence Limit: ", SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Upper Confidence Limit, Percent"),"Filtered.Value"],"%", "\n", 
                                  "Lower Confidence Limit: ", SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Lower Confidence Limit, Percent"),"Filtered.Value"], "%", "\n"))) +
      
      geom_errorbar(aes(ymin=LowerConfidenceLimit, ymax=UpperConfidenceLimit, text = paste("Value: ", Percent, "%","\n", 
                                                                                               "Upper Confidence Limit: ", UpperConfidenceLimit,"%", "\n", 
                                                                                               "Lower Confidence Limit: ", LowerConfidenceLimit, "%", "\n", 
                                                                                               LocalAuthority, ",", Filtered.DateCode))) 
      
      
    
  } else if (input$"ConfidenceIntervalsBC" == TRUE & input$"ScottishAverageBC" == FALSE) {
    
    p + geom_errorbar(aes(ymin=LowerConfidenceLimit, ymax=UpperConfidenceLimit, text = paste("Value: ", Percent, "%","\n", 
                                                                                             "Upper Confidence Limit: ", UpperConfidenceLimit,"%", "\n", 
                                                                                             "Lower Confidence Limit: ", LowerConfidenceLimit, "%", "\n", 
                                                                                             LocalAuthority, ",", Filtered.DateCode)))
    
  } else if (input$"ConfidenceIntervalsBC" == FALSE & input$"ScottishAverageBC" == TRUE) {
    
    p + geom_rect(xmin=0, xmax=100
                  , ymin=SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Upper Confidence Limit, Percent"),"Filtered.Value"]
                  , ymax=SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Lower Confidence Limit, Percent"),"Filtered.Value"]
                  , fill = "#008080"
                  , alpha = 0.1) +
    
      geom_hline(aes(yintercept = SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="Percent"),"Filtered.Value"],
                     text = paste("Scottish Average: ", SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="Percent"),"Filtered.Value"], "%","\n", 
                                  "Upper Confidence Limit: ", SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Upper Confidence Limit, Percent"),"Filtered.Value"],"%", "\n", 
                                  "Lower Confidence Limit: ", SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="95% Lower Confidence Limit, Percent"),"Filtered.Value"], "%", "\n")
      ))
    
  } else {
    p
  }
  
  p <- ggplotly(tooltip = "text")
  
})

output$map <- renderLeaflet({
  
  mapData <- merge(scotLocAuth,
                   SHS_BC(),
                   by.x = "lad16nm",
                   by.y = "Filtered.FeatureCode")
  
  factpal=colorFactor(palette = byYearColoursMap, domain=SHS_BC()$StatisticallySignificant)
  
  labels <- sprintf("<p>Value: %g&#37;<br/>
                    Upper Confidence Limit: %g&#37;<br/>
                    Lower Confidence Limit: %g&#37;<br/>
                    %s, %s<br/>
                    </p>",
                    mapData$Percent, 
                    mapData$"95% Upper Confidence Limit, Percent",
                    mapData$"95% Lower Confidence Limit, Percent",
                    mapData$lad16nm,
                    mapData$Filtered.DateCode
                    ) %>% lapply(htmltools::HTML)
  
  leaflet(mapData) %>% 
    
    setView(lng = -3.5, lat = 56.817, zoom = 4.5) %>% 
    
    addPolygons(color = "#000000",
                weight = 0.4,
                smoothFactor = 0.5,
                opacity = 1,
                fillOpacity = 1,
                fillColor = ~factpal(StatisticallySignificant),
                highlightOptions = highlightOptions(color = "#000000", weight = 1, bringToFront = TRUE),
                label = labels,
                labelOptions = labelOptions(style = list("font-weight" = "normal", padding = "3px 8px"), 
                                            textsize = "12px", direction = "auto")) %>%
    
    addLegend(pal=factpal,
              values= ~StatisticallySignificant,
              title = NULL,
              opacity = 1)
})

output$byYearTitle <- renderUI({HTML(input$LocalAuthorityServicesAndPerformanceBC)})
output$byYearSubtitle <- renderUI({paste("Colour range shows statistically significant difference from the Scottish average (",SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="Percent"), "Filtered.Value"], "%)", collapse = " ")})
output$byYearTitleMap <- renderUI({HTML(input$LocalAuthorityServicesAndPerformanceBC)})
output$byYearSubtitleMap <- renderUI({paste("Colour range shows statistically significant difference from the Scottish average (",SHS_BC_Scotland()[which(SHS_BC_Scotland()$Filtered.Measurement=="Percent"), "Filtered.Value"], "%)", collapse = " ")})

#TO DO: Add working save functionality
# observeEvent(input$saveButtonBC, {
#   wd <- choose.dir()
#   fileName <- input$userFileNameBC
#   fileType <- input$userFileTypeBC
#   setwd(wd)
#   ggsave(filename = gsub(" ", "", paste(c(fileName,".",fileType), collapse = " ")), width = 20, height = 20, units = "cm")
# })

shinyjs::onclick("openSaveButtonBC", shinyjs::toggle(id = "saveSectionBC", anim = TRUE))
shinyjs::onclick("ShowDataMeaningBC", shinyjs::toggle(id = "DataMeaningBC", anim = TRUE))
shinyjs::onclick("ShowZoomDetailsBC", shinyjs::toggle(id = "ZoomInstructionsBC", anim = TRUE))
shinyjs::onclick("ShowIsolateDataDetailsBC", shinyjs::toggle(id = "IsolateDataInstructionsBC", anim = TRUE))
shinyjs::onclick("ShowMissingDataDetailsBC", shinyjs::toggle(id = "MissingDataTextBC", anim = TRUE))
shinyjs::onclick("ShowConfidenceIntervalsDetailsBC", shinyjs::toggle(id = "ConfidenceIntervalsTextBC", anim = TRUE))
shinyjs::onclick("ShowStatisticallySignificantTextBC", shinyjs::toggle(id = "StatisticallySignificantTextBC", anim = TRUE))