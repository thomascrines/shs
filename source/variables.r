Unfiltered <- read.csv("data/serv1_all_251018.csv")

LocalAuthorityServicesAndPerformanceUnsorted <- unique(Unfiltered["Local.Authority.Services.and.Performance"])
LocalAuthorityServicesAndPerformance <- LocalAuthorityServicesAndPerformanceUnsorted[order(unique(Unfiltered["Local.Authority.Services.and.Performance"])),]

AgeUnsorted <- unique(Unfiltered["Age"])
Age <- AgeUnsorted[order(unique(Unfiltered["Age"])),]

GenderUnsorted <- unique(Unfiltered["Gender"])
Gender <- GenderUnsorted[order(unique(Unfiltered["Gender"])),]

UrbanRuralClassificationUnsorted <- unique(Unfiltered["Urban.Rural.Classification"])
UrbanRuralClassification <- UrbanRuralClassificationUnsorted[order(unique(Unfiltered["Urban.Rural.Classification"])),]

SIMDQuintilesUnsorted <- unique(Unfiltered["SIMD.quintiles"])
SIMDQuintiles <- SIMDQuintilesUnsorted[order(unique(Unfiltered["SIMD.quintiles"])),]

DateCodeUnsorted <- unique(Unfiltered["DateCode"])
DateCode <- DateCodeUnsorted[order(unique(Unfiltered["DateCode"])),]

FeatureCodeUnsorted <- unique(Unfiltered["FeatureCode"])
FeatureCode <- FeatureCodeUnsorted[order(unique(Unfiltered["FeatureCode"])),]

localAuthoritiesShape <- readOGR(dsn="data", layer="Local_Authority_Districts_December_2016_Super_Generalised_Clipped_Boundaries_in_Great_Britain", verbose=FALSE)
locAuthLatLon <- spTransform(localAuthoritiesShape, CRS("+proj=longlat +datum=WGS84"))

scotLocAuth <- subset(locAuthLatLon, locAuthLatLon$lad16nm %in% FeatureCode)

byYearColours <- c("#81c9bb","#6cafe1", "#b8b8ba")
byYearColoursMap <- c("#000000", "#81c9bb", "#6cafe1", "#b8b8ba")
timeSeriesColours <- brewer.pal(8, "Dark2")

dataMeaningText <- "The data shown represents the percentage of survey repondents who agreed or strongly agreed with the displayed statement. \n For more information about the survey methodology please see the about section."
missingDataMessage <- "Some tables include missing values. This is where the count on which percentages would be calculated is less than 50 and this data is judged to be insufficiently robust for publication."
zoomInstructionText <- "To zoom, drag the cursor over the graph. Double click on the graph to zoom out. To adjust the y-axis to the data, use the button on the left."
graphInstructionText <- "To hide a line, click once on the name of the line in the legend. This can be undone by clicking the name of the legend again. To show only one line and hide the rest, double-click the name of the desired line. To undo, double-click the name again."
confidenceIntervalsText <- "Confidence intervals can be used to estimate the precision of results obtained from our sample, compared to the true population. A 95% confidence interval is a range which we can say, with 95 per cent certainty, that the true value lies within. Caution should be taken when interpreting results with large confidence intervals as the larger the range, the less robust the result."
statisticallySignificantText <- "A difference between two areas is significant if it is so large that a difference of that size (or greater) is unlikely to have occured purely by chance. 
                                Conventionally, significance is tested at the five per cent level, which means that a difference is considered significant if it would only have occured once in 20 different samples. 
                                Testing significance involves comparing the difference between the two samples with the 95 per cent confidence limits for each of the two estimates."